import argparse


def parse_arg():
    parser = argparse.ArgumentParser(description="player simulator")
    parser.add_argument('--player1', type=int, help="id if the player wanted")
    parser.add_argument('--player2', type=int, help="id if the player wanted")
    parser.add_argument('--game', type=int, help="id of the game")

    args = parser.parse_args()
    print(args.game);
    if not args.player1 and not args.player2 and not args.game:
        parser.print_help()
        exit
    return args
