import modargparse
import dataaccess

import threading
import time
import sys


def batiment_management():
    bat = 0
    while True:
        time.sleep(2)
        bat = dataaccess.add_batiment(args.player1, args.player2, args.game, bat)
        print(bat)


def score_management():
    while True:
        time.sleep(5)
        dataaccess.add_score(args.player1, args.player2, args.game)

if __name__ == "__main__":
    try:
        args = modargparse.parse_arg()
        print("Starting simulation")

        d1 = dataaccess.check_player(args.player1)
        d2 = dataaccess.check_player(args.player2)
        if not d1 or not d2:
            print("Error: player is not present in the database: " + str(d1) + "/" + str(d2))
            sys.exit

        t_bat = threading.Thread(target=batiment_management)
        t_score = threading.Thread(target=score_management)
        t_bat.setDaemon(True)
        t_score.setDaemon(True)
        t_bat.start()
        t_score.start()
        while True:
            pass
    except KeyboardInterrupt:
        dataaccess.finish_game(args.game)
        sys.exit()
