import pymysql
import pymysql.cursors
import random
import datetime
import time


Ghost= "localhost"
Gdb = "suapolis_db"
Guser = "root"
Gpassword = "Test1234"


def create_player(name, password):
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb,
                           autocommit=True)

    a = conn.cursor()
    query = "INSERT INTO `t_users` (id, user_name, password) VALUES (NULL, \"" + name + "\", \"" + password + "\")"

    a.execute(query)

    a.close()
    conn.close()

    return a.lastrowid


def create_game(id_player1, id_player2, identifier):
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb,
                           autocommit=True)

    a = conn.cursor()
    query = "INSERT INTO `t_game` (`str_id`, `player1_id`, `player2_id`, `finish`, `seed`) VALUES (\"" + identifier + "\", \"" + str(id_player1) + "\", \"" + str(id_player2) +"\", \"" + str(1) +"\", \"1234567890\" )"

    a.execute(query)

    a.close()
    conn.close()

    return a.lastrowid


def check_player(id):
    result = False
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb)

    a = conn.cursor(pymysql.cursors.DictCursor)
    query = 'SELECT * FROM t_users WHERE id = ' + str(id)
    a.execute(query)

    for row in a:
        result = True

    a.close()
    conn.close()
    return result

def add_batiment(id_player1, id_player2, id_game, id_bat):
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb,
                           autocommit=True)

    a = conn.cursor()

    query = "INSERT INTO `t_actions` (`id`, `id_game`, `id_player`, `message`, `date`) VALUES (NULL, '"\
            + str(id_game) + "', '" + str(id_player1) + "', \"new," + str(random.randint(1, 5)) + "," + str(id_bat) + "," + str(random.randint(50, 550)) + "," + str(random.randint(50,250)) + "," + str(random.randrange(50,450)) + "\", NOW());"
    id_bat += 1
    query += "INSERT INTO `t_actions` (`id`, `id_game`, `id_player`, `message`, `date`) VALUES (NULL, '"\
            + str(id_game) + "', '" + str(id_player2) + "', \"new,"+ str(random.randint(1, 5)) + "," + str(id_bat) + "," + str(random.randint(50, 550)) + "," + str(random.randint(50,250)) + "," + str(random.randrange(50,450)) + "\", NOW());"
    a.execute(query)

    a.close()
    conn.close()

    id_bat += 1

    return id_bat


def add_score(id_player1, id_player2, id_game):
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb,
                           autocommit=True)

    a = conn.cursor(pymysql.cursors.DictCursor)

    query = "INSERT INTO `t_score` (`id`, `game_id`, `time`) VALUES (NULL, '" + str(id_game) + "', NOW())"
    a.execute(query)

    id_score = a.lastrowid

    query = "INSERT INTO `t_score_per_player` (`id`, `id_score`, `id_player`, `score`, `production`, `bonheur`, `argent`) VALUES (NULL, " + str(id_score) + ", " + str(id_player1) + ", " + str(random.randint(0, 60000)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 60000)) + "); "
    query += "INSERT INTO `t_score_per_player` (`id`, `id_score`, `id_player`, `score`, `production`, `bonheur`, `argent`) VALUES (NULL, " + str(id_score) + ", " + str(id_player2) + ", " + str(random.randint(0, 60000)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 60000)) + "); "

    a.execute(query)
    a.close()
    conn.close()

def add_batimentS(id_player1, id_player2, id_game, id_bat):
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb,
                           autocommit=True)

    a = conn.cursor()

    time_tuple = time.time()
    time_tuple += id_bat
    date_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(time_tuple))
    f = '%Y-%m-%d %H:%M:%S'

    query = "INSERT INTO `t_actions` (`id`, `id_game`, `id_player`, `message`, `date`) VALUES (NULL, '"+ str(id_game) + "', '" + str(id_player1) + "', \"new," + str(random.randint(1, 5)) + "," + str(id_bat) + "," + str(random.randint(50, 550)) + "," + str(random.randint(50,250)) + "," + str(random.randrange(50,450)) + "\", \"" + str(datetime.datetime.strptime(date_str, f)) + "\");"
    id_bat += 1
    query += "INSERT INTO `t_actions` (`id`, `id_game`, `id_player`, `message`, `date`) VALUES (NULL, '" + str(id_game) + "', '" + str(id_player2) + "', \"new,"+ str(random.randint(1, 5)) + "," + str(id_bat) + "," + str(random.randint(50, 550)) + "," + str(random.randint(50,250)) + "," + str(random.randrange(50,450)) + "\", \""+ str(datetime.datetime.strptime(date_str, f)) + "\");"
    a.execute(query)

    a.close()
    conn.close()

    id_bat += 1

    return id_bat


def add_scoreS(id_player1, id_player2, id_game, otime):
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb,
                           autocommit=True)

    a = conn.cursor(pymysql.cursors.DictCursor)

    time_tuple = time.time()
    time_tuple += otime
    date_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(time_tuple))
    f = '%Y-%m-%d %H:%M:%S'


    query = "INSERT INTO `t_score` (`id`, `game_id`, `time`) VALUES (NULL, '" + str(id_game) + "', \"" + str(datetime.datetime.strptime(date_str, f)) + "\")"
    a.execute(query)

    id_score = a.lastrowid

    query = "INSERT INTO `t_score_per_player` (`id`, `id_score`, `id_player`, `score`, `production`, `bonheur`, `argent`) VALUES (NULL, " + str(id_score) + ", " + str(id_player1) + ", " + str(random.randint(0, 60000)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 60000)) + "); "
    query += "INSERT INTO `t_score_per_player` (`id`, `id_score`, `id_player`, `score`, `production`, `bonheur`, `argent`) VALUES (NULL, " + str(id_score) + ", " + str(id_player2) + ", " + str(random.randint(0, 60000)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 100)) + ", " + str(random.randint(0, 60000)) + "); "

    a.execute(query)
    a.close()
    conn.close()

def finish_game(id_game):
    conn = pymysql.connect(host=Ghost,
                           user=Guser,
                           password=Gpassword,
                           db=Gdb,
                           autocommit=True)

    a = conn.cursor(pymysql.cursors.DictCursor)

    query = "UPDATE `t_game` SET `finish` = '1' WHERE `t_game`.`id` = " + str(id_game)

    a.execute(query)
    a.close()
    conn.close()