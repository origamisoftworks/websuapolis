import dataaccess
import time

print(">Starting add_data")
player_dic = {}
print("Adding players:")
print("Status : ", end='', flush=True)
for i in range(0, 150):
    player_dic[i] = dataaccess.create_player("TestG" + str(i), "testg")
    print(">", end='', flush=True)
print(" ")
print("OK")

print("Adding games with data:")
print("Status : ", end='', flush=True)
for i in range(0, 75):
    player_1 = player_dic[i]
    player_2 = player_dic[i + 75]
    id_game = dataaccess.create_game(player_1, player_2, "0000000X" + str(i))
    id_bat = 0
    for j in range(0, 100):

        if j % 2 == 0:
            id_bat = dataaccess.add_batimentS(player_1, player_2, id_game, id_bat)
        if j % 5 == 0:
            dataaccess.add_scoreS(player_1, player_2, id_game, id_bat)
    print(">", end='', flush=True)
print(" ")
print("OK")
print("Script finished")
