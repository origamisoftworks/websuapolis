# README #

This is the repository for the website of our PLIC Suapolis.

To use this website please follow these instructions:

### Backend ###

* In src/backend:
	- use "npm install"
* In your MySQL:
	- create a database named 'suapolis_db'
	- use the script in src/backend/database/suapolis_db.sql to generate the table
* Change the file config.json to make an access to the database

### Frontend ###

* In src/frontend:
	- use "npm install"

### Test ###

* In test/
	- install **python3.5** and **pip3**
	- use *pip3 install -r requirement.txt* to install the module
	- change the beginning of the **dataaccess.py** to match with your MySQL configuration

## How it work: ##

To launch the website:
- In src/backend:
	- use *npm start*
* In src/frontend:
	- use *npm start*

You have two scripts in the test folder:
- The script **adddata.py** will correctly fill your database with data. To use it, please use *python ./adddata*
- The script **simulator.py** will play a game. To use it, please make
  *python ./simulator.py --player1 player_id --player2 player_id --game game_id* which
  are correctly inserted before in the database and where finish column's value is 0