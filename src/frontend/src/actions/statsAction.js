export function resetInfos()
{
    return {
        type: "RESET_INFOS",
        payload: {}
    }
}

export function addInfo(info)
{
    return {
        type: "ADD_INFO",
        payload: info
    }
}