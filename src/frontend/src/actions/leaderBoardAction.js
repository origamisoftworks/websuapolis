/**
 * Created by Quentin on 07/06/2017.
 */
export function setFilter(searchFilter)
{
    return {
        type: "CHANGE_FILTER",
        payload: searchFilter
    }
}

export function setCursor(cursor)
{
    return {
        type: "SET_CURSOR",
        payload: cursor
    }
}

export function setDatasCount(count)
{
    debugger;
    return {
        type: "SET_DATAS_COUNT",
        payload: count
    }
}