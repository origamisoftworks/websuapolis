export function resetPoints()
{
    return {
        type: "RESET_POINTS",
        payload: {}
    }
}

export function addPoint(point)
{
    return {
        type: "ADD_POINT",
        payload: point
    }
}