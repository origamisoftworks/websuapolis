/**
 * Created by Quentin on 09/06/2017.
 */
export function updateDatas(datas)
{
    return {
        type: "SET_JSON",
        payload: datas
    }
}