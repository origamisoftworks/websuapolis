export function setSelected(pos)
{
    return {
        type: "SET_SELECTED",
        payload: pos
    }
}

export function pause()
{
    return {
        type: "PAUSE",
        payload: {}
    }
}

export function play()
{
    return {
        type: "PLAY",
        payload: {}
    }
}

export function stop()
{
    return {
        type: "STOP",
        payload: {}
    }
}