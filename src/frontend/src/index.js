import React from 'react';
import ReactDOM from 'react-dom';

import Routes from './Routes';
import Nav_bar from './header_components/nav_bar';

ReactDOM.render(React.createElement(Nav_bar), document.getElementById('header'));

ReactDOM.render(React.createElement(Routes), document.getElementById('content'));
