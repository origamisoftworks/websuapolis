import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import ApiMiddleware from '../middleware/ApiMiddleware'

import reducer from '../reducers';

const middleware = applyMiddleware(thunk, ApiMiddleware);

const store = createStore(
    reducer,
    compose(middleware, window.devToolsExtension ? window.devToolsExtension() : f => f)
);

export default store;