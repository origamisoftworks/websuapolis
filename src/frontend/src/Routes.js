import React from 'react';
import { Provider } from 'react-redux';

//import Store from './store/store';
import MyRouter from './router/router'

const Routes = () => (
	<MyRouter />
)

export default Routes