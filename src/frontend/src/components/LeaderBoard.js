/**
 * Created by Quentin on 06/06/2017.
 */
import React from 'react';
import store from '../store/store';
import { Provider } from 'react-redux';

import LeaderBoard_content from './LeaderBoard/content';

require('./LeaderBoard.css');

const LeaderBoard = () => (
    <Provider store={store}>
        <div className="center-widget widget-container">
            <h4 className="widget-header leaderboard_title">
                <img className="icon_title" src="https://www.exophase.com/assets/zeal/leaderboards/leaderboard-platinum-trophy-header.png" />
                <b>Leaderboard</b>
            </h4>

            <LeaderBoard_content/>

        </div>
    </Provider>
);

export default LeaderBoard