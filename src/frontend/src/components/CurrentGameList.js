/**
 * Created by Quentin on 07/06/2017.
 */
import React from 'react';
import store from '../store/store';
import { Provider } from 'react-redux';

import GameList from "./CurrentGameList/GameList";

require('./CurrentGameList.css');

const CurrentGameList = () => (

    <Provider store={store}>
        <div className="center-widget widget-container">
            <h4 className="widget-header game_list_title">
                <img className="icon_title" src="https://s.sharecare.com/static/v5.25.1/img/themes/sharecare/play-overlay.png" />
                <b>Parties en cours</b>
            </h4>

            <GameList />
        </div>
    </Provider>
);

export default CurrentGameList;