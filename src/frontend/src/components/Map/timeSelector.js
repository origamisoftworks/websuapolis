import React from 'react';
import { bindActionCreators, dispatch} from 'redux';
import * as selectAction from '../../actions/selectAction'
import { connect } from 'react-redux';
import store from '../../store/store';


function mapStateToProps(state) {
    return {
        size: state.stats.length - 1,
        time: state.stats[state.select.pos],
        status: state.select.pause,
        stop: state.select.stop
    };
}

function formatDate(date) {
    var monthNames = [
        "Janvier", "Février", "Mars",
        "Avril", "Mai", "Juin", "Juillet",
        "Août", "Septembre", "Octobre",
        "Novembre", "Décember"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' à ' + hours + ':' + minutes + ':' + seconds;
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(selectAction, dispatch)
    };
}

function rand(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}

let id = 0;

function updateData()
{
    store.dispatch({type: 'UPDATE_DATAS_MAP', payload: {}})
}


let interval = null;

class TimeSelector extends React.Component {


    constructor(props) {
        super(props);
    }

    componentWillMount()
    {
        updateData();
    }

    componentDidMount()
    {
        interval = setInterval(updateData, 2000);
    }

    changeTime(event)
    {
        this.props.actions.setSelected(event.target.value);
    }

    changeStatus()
    {
        if (this.props.status == false)
            this.props.actions.pause();
        else
        {
            this.props.actions.setSelected(0);
            this.props.actions.play();
        }
    }

    render() {
        let date = this.props.time != null ? formatDate(new Date(this.props.time.date)) : "Chargement...";
        let time_text = !this.props.status ? "En direct" : "Replay";
        let class_name = this.props.stop ? "stop" : (this.props.status ? "play" : "pause");
        id = this.props.id;

        //                <button onClick={this.test.bind(this)}>test</button>
        return (
            <div className="center-widget widget-container">
                <div className="time widget-header">{time_text} : {date}</div>
                <button className={class_name} onClick={this.changeStatus.bind(this)}/>
                <input type="range" disabled={!this.props.status} onChange={this.changeTime.bind(this)} min="0" max={this.props.size} step="1" defaultValue="0"  />
            </div>
        );

    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TimeSelector);