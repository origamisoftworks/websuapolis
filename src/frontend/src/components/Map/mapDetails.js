import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as action from '../../actions/pointsAction'

function mapStateToProps(state) {
    return {
        points: state.points,
        timestamp: state.stats[state.select.pos].date
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    };
}


class MapDetails extends React.Component {
    render() {
        let rows = [];
        let i = 0;
        let player = this.props.player;
        let time = new Date(this.props.timestamp).getTime();
        this.props.points.forEach(function(point) {
            let time2 = new Date(point.construction_date).getTime();
            let time3 = new Date(point.destruction_date).getTime();
            //console.log("compare " + player + " with " + point.player + "," + time + " with " + time2 + " and " + point.destruction_date );
            if (player == point.player && time >= time2 && (time <= time3 || point.destruction_date == null))
            {
                let icon = null;
                switch(point.type)
                {
                    case "1":
                        icon = "https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678085-house-128.png";
                        break;
                    case "2":
                        icon = "http://www.iconarchive.com/download/i103361/paomedia/small-n-flat/building.ico";
                        break;
                    case "3":
                        icon = "https://maxcdn.icons8.com/Share/icon/color/Industry//wind_turbine1600.png";
                        break;
                    case "4":
                        icon = "http://www.iconsfind.com/wp-content/uploads/2015/11/20151109_56400effe0621-210x249.png";
                        break;
                    case "5":
                        icon = "https://d30y9cdsu7xlg0.cloudfront.net/png/122071-200.png";
                        break;
                    default:
                        icon = "https://d30y9cdsu7xlg0.cloudfront.net/png/250091-200.png";
                        break;
                }
                rows.push(<image className="buildings" key={i} x={point.x} y={point.z} width="20" height="20" xmlnsXlink="http://www.w3.org/1999/xlink" xlinkHref={icon} />);
            }
            ++i;
        });

        return (
            <div id="map_j1">
                <div className="title_map">
                    Map
                </div>
                <svg width="600" height="500" className="map">
                    <image xmlnsXlink="http://www.w3.org/1999/xlink" xlinkHref="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Map_of_Iceland.svg/330px-Map_of_Iceland.svg.png" x="0" y="0" width="600" height="500"/>
                    <g className="buildings">
                        {rows}
                    </g>
                </svg>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MapDetails);