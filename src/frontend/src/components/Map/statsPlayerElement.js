import React from 'react';

class StatsPlayerElement extends React.Component {
    render() {
        return (
            <ul className="value-list info_block">
                <li>
                    <div className="description">
                        {this.props.name}
                    </div>
                    <div className="value">
                        <span>{this.props.value}</span>
                        <span className="positive small">
							{this.props.complement}
						</span>
                    </div>
                </li>
            </ul>
        );
    }
}

export default StatsPlayerElement;