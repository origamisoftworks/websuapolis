import React from 'react';
import StatsPlayerElement from './statsPlayerElement';

class StatsPlayer extends React.Component {
    render() {
        return (
            <div className="widget-content">
                <StatsPlayerElement name="Argent" value={this.props.money} complement="€" />
                <StatsPlayerElement name="Score" value={this.props.score} />
                <StatsPlayerElement name="Production" value={this.props.production} />
                <StatsPlayerElement name="Bonheur" value={this.props.bonheur} />
            </div>
        );
    }
}

export default StatsPlayer;