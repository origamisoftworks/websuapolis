import React from 'react';
import { bindActionCreators } from 'redux';
import * as action from '../../actions/statsAction'
import { connect } from 'react-redux';
import MapDetails from './mapDetails'
import StatsPlayer from './statsPlayer';

function mapStateToProps(state) {
    return {
        infos: state.stats[state.select.pos],
        players : state.player
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    };
}

class Player extends React.Component {

    render() {
        if (this.props.players == null)
            return (<div></div>);

        console.log(this.props);

        if (this.props.infos == null)
        {
            return (<div></div>);
        }
        let pos = this.props.number == 1 ? "fleft widget-container" : "fright widget-container";
        let score = this.props.number == 1 ? this.props.infos.j1.score : this.props.infos.j2.score;
        let money = this.props.number == 1 ? this.props.infos.j1.money : this.props.infos.j2.money;
        let production = this.props.number == 1 ? this.props.infos.j1.production : this.props.infos.j2.production;
        let bonheur = this.props.number == 1 ? this.props.infos.j1.bonheur : this.props.infos.j2.bonheur;

        let player = this.props.number == 1 ? this.props.players.j1.id : this.props.players.j2.id;
        let pseudo = this.props.number == 1 ? this.props.players.j1.pseudo : this.props.players.j2.pseudo;

        return (
            <div className={pos}>
                <div className="widget-header">
                    {pseudo}
                </div>
                <StatsPlayer money={money} score={score} production={production} bonheur={bonheur} />

                <MapDetails player={player} />
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Player);