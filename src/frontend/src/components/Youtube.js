import React, { Component, PropTypes } from 'react'

let loadYT

export default class YouTube extends Component {
    componentDidMount () {
        if (!loadYT) {
            loadYT = new Promise((resolve) => {
                const tag = document.createElement('script')
                tag.src = 'https://www.youtube.com/iframe_api'
                const firstScriptTag = document.getElementsByTagName('script')[0]
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)
                window.onYouTubeIframeAPIReady = () => resolve(window.YT)
            })
        }
        loadYT.then((YT) => {
            this.player = new YT.Player('muteYouTubeVideoPlayer', {
                    videoId: 'hMlKSxkl7vE', // YouTube Video ID
                    width: window.screen.availWidth,               // Player width (in px)
                    height: window.screen.availHeight + 200,              // Player height (in px)
                    playerVars: {
                        autoplay: 1,        // Auto-play the video on load
                        controls: 0,        // Show pause/play buttons in player
                        showinfo: 0,        // Hide the video title
                        modestbranding: 1,  // Hide the Youtube Logo
                        loop: 1,            // Run the video in a loop
                        fs: 0,              // Hide the full screen button
                        cc_load_policy: 0, // Hide closed captions
                        iv_load_policy: 3,  // Hide the Video Annotations
                        playlist: 'hMlKSxkl7vE',
                        autohide: 0         // Hide video controls when playing
                    },
                    events: {
                        onReady: function (e) {
                            e.target.mute();
                        }
                    }
                });
        })
    }

    render () {
        return (
            <div ref={(r) => { this.youtubePlayerAnchor = r }}></div>
        )
    }
}

YouTube.propTypes = {
    YTid: PropTypes.string.required,
    width: PropTypes.number,
    height: PropTypes.number,
    onStateChange: PropTypes.func
}