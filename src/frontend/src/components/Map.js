import React from 'react';
import store from '../store/store';
import { Provider } from 'react-redux';
import Player from './Map/player';
import TimeSelector from './Map/timeSelector'

import json from './test.json';

require('./map.css');

const Map = ({ match }) => {

    store.dispatch({type: 'SET_ID', payload: match.params.id});
	return (
		<Provider store={store}>
		<div id="page_content">
			<TimeSelector/>
			<div id="top_players">
				<Player number="1"/>
				<Player number="2"/>
			</div>
		</div>
	</Provider>
	);
};

export default Map;