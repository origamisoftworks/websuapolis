/**
 * Created by Quentin on 06/06/2017.
 */
import React from 'react';
import store from '../store/store';
import { Provider } from 'react-redux';

import Profile_content from './Profile/ProfileContent';

require('./LeaderBoard.css');

const Profile = ({ match }) => {
    let id = match.params.id;
    return (
    <Provider store={store}>
        <Profile_content id_player={id}/>
    </Provider>
    );
};

export default Profile