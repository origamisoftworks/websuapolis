import React from 'react';
import Youtube from './Youtube';

require('./home.css');

import logo from '../header_components/ressources/logo/logo.png';
import logo_transparent from '../header_components/ressources/logo/logo_transparent.png';

const Home = () => (
	<div>
		<section className="highlight  highlight--normal custom-highlight__abgo__keyart" id="top">
			<div className="highlight__background__image  highlight__background__image--cover animated dollyIn background_home">
			</div>

			<div className="highlight__container highlight__content highlight__container--no-image">
				<img src={logo_transparent} alt="SuaPolis"/>
					<div>
						<button className="btn btn-4 btn-4b icon-arrow-left">↓ Windows</button>
						<button className="btn btn-4 btn-4b icon-arrow-left">↓ Mac</button>
						<button className="btn btn-4 btn-4b icon-arrow-left">↓ Linux</button>
					</div>
			</div>
		</section>

		<div className="separator">
			<img className="separator__image" src="https://d21tktytfo9riy.cloudfront.net/wp-content/uploads/2016/02/30123909/abgo-separator-02.png" alt="" />
		</div>

		<section className="highlight  highlight--normal custom-highlight__abgo__keyart" id="video">
			<div className="highlight__background__image  highlight__background__image--cover animated dollyIn">
				<article className="panel" id="galeria">
					<div id="muteYouTubeVideoPlayer"></div>
					<script async src="https://www.youtube.com/iframe_api"></script>
					<Youtube />
				</article>

				<div className="highlight__background__overlay">
				</div>
			</div>

			<div className="highlight__container highlight__content highlight__container--no-image">
				<br/><br/><br/><br/><br/><br/><br/><br/>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			</div>
		</section>

		<div className="separator">
			<img className="separator__image" src="https://d21tktytfo9riy.cloudfront.net/wp-content/uploads/2016/02/30123909/abgo-separator-02.png" alt=""/>
		</div>

		<section className="highlight  highlight--normal custom-highlight__abgo__keyart" id="cara">
			<div className="highlight__background__image  highlight__background__image--cover animated dollyIn">
			</div>

			<div className="highlight__container highlight__content highlight__container--no-image background_home">
				A venir :)<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			</div>
		</section>

		<div className="separator">
			<img className="separator__image" src="https://d21tktytfo9riy.cloudfront.net/wp-content/uploads/2016/02/30123909/abgo-separator-02.png" alt=""/>
		</div>

		<section className="highlight  highlight--normal custom-highlight__abgo__keyart" id="cara">
			<div className="highlight__container highlight__content highlight__container--no-image black_background">
				Crédits<br/><br/>
			</div>
		</section>
	</div>
);


export default Home