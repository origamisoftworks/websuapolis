/**
 * Created by Quentin on 06/06/2017.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import * as action from '../../actions/leaderBoardAction'
import { connect } from 'react-redux';

import Datas from './datas'

require('../../bootstrap/bootstrap.min.css');
require('./LeaderBoard.css');


function mapStateToProps(state) {
    return {
        searchFilter: state.leaderBoard.searchFilter,
        json_leaderboard_datas: state.leaderBoardJson
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    };
}

class LeaderBoard_content extends React.Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);


    }

    handleChange(event)
    {
        this.props.actions.setFilter(event.target.value);
        this.updateDatasCount(event.target.value);
    }

    updateDatasCount(filter)
    {
        debugger;
        var json = this.props.json_leaderboard_datas;

        var count = 0;
        for (var i = 0; i < json.players.length; ++i) // count elements in json
        {
            if (json.players[i].name.indexOf(filter) !== 0)
                continue;
            ++count;
        }
        debugger;
        this.props.actions.setDatasCount(count);
        this.props.actions.setCursor(0);
    }

    handleSubmit(event)
    {
        event.preventDefault();
    }

    render()
    {
        return(
                <div>
                    <div className="text-center small-padding-bottom">
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" className="search-box" name="search" placeholder="Rechercher"
                                   value={this.props.searchFilter} onChange={this.handleChange}/>
                        </form>
                    </div>

                    <div className="col-lg-8 offset-lg-2">
                        <Datas />
                    </div>

                </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LeaderBoard_content);