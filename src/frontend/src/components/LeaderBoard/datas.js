/**
 * Created by Quentin on 06/06/2017.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import store from '../../store/store';
import * as action from '../../actions/leaderBoardAction';
import { connect } from 'react-redux';

require('./LeaderBoard.css');
require('../../bootstrap/bootstrap.min.css');

var leaderboard_display_count = 100;

function loadDatas()
{
    fetch('http://localhost:4242/api/leaderboard')
        .then(function(response) {
            response.json().then(function(data)
            {
                store.dispatch({type: "SET_LEADERBOARD_JSON", payload: data});
                store.dispatch({type: "SET_DATAS_COUNT", payload: data.players.length});
            });
        });
}

function mapStateToProps(state) {
    return {
        searchFilter: state.leaderBoard.searchFilter,
        cursor: state.leaderBoard.cursor,
        datas_count: state.leaderBoard.datas_count,
        json_leaderboard_datas: state.leaderBoardJson
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    };
}

class Datas extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            canNext: true,
            canPrevious: false
        };
    }

    componentWillMount()
    {
        loadDatas();
    }

    showNext()
    {
        this.props.actions.setCursor(this.props.cursor + leaderboard_display_count);
    }

    showPrevious()
    {
        this.props.actions.setCursor(this.props.cursor - leaderboard_display_count);
    }

    render() {

        let canNext = false;
        let canPrevious = false;
        if (this.props.cursor < leaderboard_display_count)
        {
            canPrevious = false;
        }
        else
        {
            canPrevious = true;
        }
        if (this.props.cursor >= this.props.datas_count - leaderboard_display_count)
        {
            canNext = false;
        }
        else
        {
            canNext = true;
        }


        return (
            <div>
                <div className="btn-div">
                    <button className="btn-style" onClick={this.showPrevious.bind(this)} disabled={!canPrevious}>
                        Afficher les 100 précédents
                    </button>
                    <button className="btn-style" onClick={this.showNext.bind(this)} disabled={!canNext}>
                        Afficher les 100 suivants
                    </button>
                </div>
                <div className="hline-leaderboard">
                </div>
                <div className="row table_header">
                    <div className="col-sm-2 text-center">
                        <b>Place</b>
                    </div>
                    <div className="col-sm-5">
                        <b>Nom</b>
                    </div>
                    <div className="col-sm-2 text-center">
                        <b>Score</b>
                    </div>
                    <div className="col-sm-3 text-center">
                        <b>Date</b>
                    </div>
                </div>
                <div className="scrollable scrollbar-track-piece scrollbar-thumb scrollbar-track scrollbar-background ">
                    <Product_table filterText={this.props.searchFilter} cursor={this.props.cursor}
                    />
                </div>
            </div>
        );
    }
}

class GenerateLeaderboard extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {
        var json = this.props.json_leaderboard_datas;

        var rows = [];
        var filter = this.props.filterText;

        var maxc = 0;
        for (var i = this.props.cursor; i < json.players.length && maxc < leaderboard_display_count; ++i)
        {
            if (json.players[i].name.indexOf(filter) !== 0)
                continue;
            ++maxc;

            var tmp = json.players[i].date;
            var splitted = tmp.split('T');
            json.players[i].date = splitted[0];

            rows.push(json.players[i]);
        }

        return (
            <div>
                {rows.map((item) =>
                    <div key={item.place.toString()} className="font-text">
                        <Product_row place={item.place} id={item.id} name={item.name} score={item.score} date={item.date} />
                    </div>
                )}
            </div>
        );
    }
}

class Product_row extends React.Component {

    render() {

        return (
            <div className="row color-gradient">
                <div className="col-sm-2 text-center">
                    {this.props.place + 1}
                </div>
                <div className="col-sm-5">
                    <a className="link" href={"profile/" + this.props.id}>
                        {this.props.name}
                    </a>
                </div>
                <div className="col-sm-2 text-center">
                    {this.props.score}
                </div>
                <div className="col-sm-3 text-center">
                    {this.props.date}
                </div>
            </div>
        );
    }
}

var Product_table = connect(
    mapStateToProps,
    mapDispatchToProps
)(GenerateLeaderboard);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Datas);

