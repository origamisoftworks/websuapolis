
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import store from '../../store/store';

import Element from '../Map/statsPlayerElement'
import ProfileGames from './ProfileGames';

require('../../bootstrap/bootstrap.min.css');

function loadDatas(id_player)
{
    fetch('http://localhost:4242/api/player/' + id_player)
        .then(function(response) {
            response.json().then(function(data)
            {
                store.dispatch({type: "SET_PROFILE", payload: data});
            });
        });
}

function mapStateToProps(state) {
    return {
        json_datas: state.profile
    };
}

class Profile extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount()
    {
        loadDatas(this.props.id_player);
    }

    render() {
        return (
            <div>
                <Product_table />
            </div>
        );
    }
}

class generate_datas extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {

        if (this.props.json_datas.nodata === true)
            return(<div></div>);

        var json =  this.props.json_datas;

        var rows = [];

        for (var i = 0; i < json.game.length; ++i)
        {
            var tmp = json.game[i].start_time;
            var splitted = tmp.split('T');
            var end = splitted[1].substring(0,5);
            var toDisplay = splitted[0] + " à " + end;
            json.game[i].start_time = toDisplay;

            rows.push(json.game[i]);
        }

        return (
            <div>
                <div className="center-widget widget-container">
                    <h4 className="widget-header leaderboard_title">Stats de {json.player.pseudo}</h4>

                    <div className="widget-content">
                        <Element name="Meilleur score" value={json.player.best_score} />
                        <Element name="Score moyen" value={json.player.average.score} />
                        <Element name="Production moyenne" value={json.player.average.production} />
                        <Element name="Bonheur moyen" value={json.player.average.bonheur} />
                        <Element name="Argent en moyenne" value={json.player.average.argent} />
                    </div>
                </div>

                <div className="center-widget widget-container">
                    <h4 className="widget-header">Parties</h4>

                    <div className="col-sm-8 offset-sm-2">
                        <div className="container-fluid custom-line">

                            <div className="row text-center">
                                <div className="col-sm-10">
                                    <div className="row">
                                        <div className="col-sm-5">
                                            <b>Score</b>
                                        </div>
                                        <div className="col-sm-5">
                                            <b>Date de fin</b>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-2">
                                    <b>Voir la partie</b>
                                </div>
                            </div>
                        </div>
                        <div className="data-window scrollbar-background scrollbar-track scrollbar-thumb scrollbar-track-piece">
                            {
                                rows.map(item =>
                                    <div key={item.id_game.toString()}>
                                        <ProfileGames id_game={item.id_game}
                                                      start_time={item.start_time}
                                                      current_score={item.current_score}
                                        />
                                    </div>
                                )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



var Product_table = connect(
    mapStateToProps
)(generate_datas);

export default Profile;