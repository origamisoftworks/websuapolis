/**
 * Created by Quentin on 08/06/2017.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import binocular from './ressources/binoculars.png';

require('../../bootstrap/bootstrap.min.css');
require('./Profile.css');

class Game extends React.Component {

    render() {
        return (
            <div className="container-fluid custom-line">

                <div className="row text-center">
                    <div className="col-sm-10">
                        <div className="row">
                            <div className="col-sm-5">
                                {this.props.current_score}
                            </div>
                            <div className="col-sm-5">
                                {this.props.start_time}
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-2">
                        <a href={"/Map/" + this.props.id_game}>
                            <img src={binocular} width={64} height={64}/>
                        </a>
                    </div>
                </div>

                <div className="hline"></div>
            </div>
        );
    }
}

export default Game;