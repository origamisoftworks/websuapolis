/**
 * Created by Quentin on 07/06/2017.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import store from '../../store/store';

import Game from './Game';

require('../../bootstrap/bootstrap.min.css');
require('./GameList.css');

function loadDatas()
{
    fetch('http://localhost:4242/api/game')
        .then(function(response) {
            response.json().then(function(data)
            {
                store.dispatch({type: "SET_JSON", payload: data});
            });
        });
}

function mapStateToProps(state) {
    return {
        json_datas: state.gameListJson
    };
}

class GameList extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount()
    {
        loadDatas();
    }

    componentDidMount()
    {
        setInterval(loadDatas, 10000); // 10 sec
    }

    render() {
        return (
            <div>
                <Product_table />
            </div>
        );
    }
}

class generate_datas extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {

        var json =  this.props.json_datas;

        var rows = [];


        for (var i = 0; i < json.games.length; ++i)
        {
            var tmp = json.games[i].start_time;
            var splitted = tmp.split('T');
            var end = splitted[1].substring(0,5);
            var toDisplay = splitted[0] + " à " + end;
            json.games[i].start_time = toDisplay;

            rows.push(json.games[i]);

        }

        let current_game_count = "Aucune partie en cours";
        if (json.game_number > 0)
            current_game_count = json.game_number + " parties en cours";

        return (
            <div>
                <div className="current-game">
                     {current_game_count}
                </div>
                <div className=" data-window scrollbar-background scrollbar-track scrollbar-thumb scrollbar-track-piece">
                    <div className="col-sm-10 offset-sm-1">
                        {
                            rows.map(item =>
                                <div key={item.id_game.toString()}>
                                    <Game id_game={item.id_game}
                                          start_time={item.start_time}
                                          player1_pseudo={item.player1.pseudo}
                                          player1_id={item.player1.id}
                                          player1_current_score={item.player1.current_score}
                                          player2_pseudo={item.player2.pseudo}
                                          player2_id={item.player2.id}
                                          player2_current_score={item.player2.current_score}
                                    />
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}



var Product_table = connect(
    mapStateToProps
)(generate_datas);

export default GameList;