/**
 * Created by Quentin on 09/06/2017.
 */
export default function reducer (state={players:[]}, action) {
    switch (action.type) {
        case 'SET_LEADERBOARD_JSON':
            return action.payload;
        default:
            return state;
    }
};