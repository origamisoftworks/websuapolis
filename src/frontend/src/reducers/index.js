import { combineReducers } from 'redux';

import stats from "./statsReducer"
import points from "./pointsReducer"
import leaderBoard from "./LeaderBoard"
import leaderBoardJson from "./LeaderBoardJsonReducer"
import player from './playerReducer'
import select from "./selectReducer"
import gameListJson from "./gameListReducer"
import profile from "./profileReducer"

export default combineReducers({
    stats,
    points,
    leaderBoard,
    leaderBoardJson,
    player,
    select,
    gameListJson,
    profile
});