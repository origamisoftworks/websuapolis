const player = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_INFO':
            return {
                date : action.payload.date,
                j1 :
                    {
                        score: action.payload.j1.score,
                        money: action.payload.j1.money,
                        production: action.payload.j1.production,
                        bonheur: action.payload.j1.bonheur,
                    },
                j2 :
                    {
                        score: action.payload.j2.score,
                        money: action.payload.j2.money,
                        production: action.payload.j2.production,
                        bonheur: action.payload.j2.bonheur,
                    },
            };
        default:
            return state;
    }
};

export default function reducer(state=[], action) {

        switch(action.type)
        {
            case 'ADD_INFO':
                return [
                    ...state,
                    player(undefined, action)
                ];
            case 'RESET_INFOS':
                return [];
        }
        return state;
}