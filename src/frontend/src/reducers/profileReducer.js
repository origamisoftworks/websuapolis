export default function reducer(state={nodata : true}, action) {

        switch(action.type)
        {
            case 'SET_PROFILE':
                return action.payload;
            case 'RESET_PROFILE':
                return {};
        }
        return state;
}