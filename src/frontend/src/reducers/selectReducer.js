export default function reducer(state={pos: 0, pause: false, stop: false}, action) {

        switch(action.type)
        {
            case 'SET_SELECTED':
                return {
                    ...state,
                    pos: action.payload
                };
            case 'PAUSE':
                return {
                    ...state,
                    pause: true
                };
            case 'PLAY':
                return {
                    ...state,
                    pause: false
                };
            case 'STOP':
            {
                return {
                    ...state,
                    pause: true,
                    stop: true
                }
            }
            case 'SET_ID':
                return {
                    ...state,
                    id: action.payload
                };
        }
        return state;
}