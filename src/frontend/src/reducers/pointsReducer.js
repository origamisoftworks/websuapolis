const point = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_POINT':
            return {
                x: action.payload.pos.x,
                y: action.payload.pos.y,
                z: action.payload.pos.z,
                id: action.payload.id,
                type: action.payload.type,
                player: action.payload.player,
                construction_date: action.payload.construction_date,
                destruction_date: action.payload.destruction_date
            };
        default:
            return state;
    }
};

export default function reducer (state=[], action) {
    switch (action.type) {
        case 'ADD_POINT':
            return [
                ...state,
                point(undefined, action)
            ];
        case 'RESET_POINTS':
            return [];
        default:
            return state;
    }
};