export default function reducer(state={pos: 0}, action) {

        switch(action.type)
        {
            case 'SET_PLAYERS':
                return {
                    "j1": {
                        "id": action.payload.j1.id,
                        "pseudo": action.payload.j1.pseudo
                    },
                    "j2": {
                        "id": action.payload.j2.id,
                        "pseudo": action.payload.j2.pseudo
                    }
                };
            case 'RESET_PLAYERS':
                return {};
        }
        return state;
}