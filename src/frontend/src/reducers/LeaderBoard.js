/**
 * Created by Quentin on 06/06/2017.
 */
export default function reducer(state={
    searchFilter: "",
    cursor: 0,
    datas_count: 0
}, action) {

    switch(action.type)
    {
        case "CHANGE_FILTER": {
            return {...state, searchFilter: action.payload};
        }
        case "SET_CURSOR":
        {
            return {...state, cursor: action.payload};
        }
        case "SET_DATAS_COUNT":
        {
            return {...state, datas_count: action.payload};
        }
    }
    return state;
}