/**
 * Created by Quentin on 08/06/2017.
 */
export default function reducer (state={games:[]}, action) {
    switch (action.type) {
        case 'SET_JSON':
            return action.payload;
        default:
            return state;
    }
};