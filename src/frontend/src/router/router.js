/**
 * Created by Quentin on 06/06/2017.
 */
import React from 'react';
import Home from '../components/Home';
import LeaderBoard from '../components/LeaderBoard';
import Map from '../components/Map';
import Profile from '../components/Profile';
import CurrentGameList from '../components/CurrentGameList';
import { BrowserRouter , Route, Switch} from 'react-router-dom';

const MyRouter = () => (
    <BrowserRouter>
        <Switch>
            <Route path="/" exact component={Home}/>
            <Route name="home" path="/home" component={Home}/>
            <Route name="Leaderboard" path="/leaderboard" component={LeaderBoard}/>
            <Route name="Currentgames" path="/currentgames" component={CurrentGameList}/>
			<Route name="Map" path="/map/:id" component={Map}/>
            <Route name="Profile" path="/profile/:id" component={Profile}/>
        </Switch>
    </BrowserRouter>
);

export default MyRouter