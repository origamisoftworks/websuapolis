import React from 'react';
import ReactDOM from 'react-dom';

import logo from './ressources/logo/logo.png';

require('./header.css');

var divStyle = {
    backgroundImage: "url(" + logo + ")"
}

const Nav_bar = () => (
		<div className="menu">
			<a href="/" title="SuaPolis" style={divStyle} className="nav-bar__logo menu">Sua Polis</a>
			<div>
				<nav role="navigation">
					<div className="menu-primary-menu-container">
						<ul id="menu-primary-menu" className="nav__list nav__list--bar">
							<li id="menu-item-15" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-15">
								<a href="/">Accueil</a>
							</li>
							<li id="menu-item-18" className="menu-item menu-item-type-post_type menu-item-object-page current-page-ancestor menu-item-18 current-menu-item">
								<a href="/currentgames">Parties en cours</a>
							</li>
							<li id="menu-item-17" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-17">
								<a href="/leaderboard" >Top</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>

)//<img src={logo} width={160} height={120}/>
export default Nav_bar