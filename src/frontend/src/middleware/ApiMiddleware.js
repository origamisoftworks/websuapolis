import * as statsAction from '../actions/statsAction'
import * as playerAction from '../actions/playerAction'
import * as pointsAction from '../actions/pointsAction'
import * as selectAction from '../actions/selectAction'

function custom_sort(a, b) {
    return new Date(b.date).getTime() - new Date(a.date).getTime();
}

function parse(json, next)
{
    next(statsAction.resetInfos());
    next(pointsAction.resetPoints());

    next(playerAction.setPlayer(json.player));
    let arr = json.statime;
    arr.sort(custom_sort);
    arr.forEach(function(point) {
        next(statsAction.addInfo(point));
    });
    json.building.forEach(function(point) {
        next(pointsAction.addPoint(point));
    });

    if (json.isfinish == "1")
        next(selectAction.stop());
}

const ApiMiddleware = store => next => action => {
    /*
     Pass all actions through by default
     */
    next(action);
    switch (action.type) {
        case 'UPDATE_DATAS_MAP':
            /*
             In case we receive an action to send an API request, send the appropriate request
             */
            if (!store.getState().select.pause)
                fetch('http://localhost:4242/api/game/' + store.getState().select.id)
                    .then(function(response) {
                        response.json().then(function(data) {
                            parse(data, next);
                        });
                    });
            break;
        /*
         Do nothing if the action does not interest us
         */
        default:
            break
    }

};

export default ApiMiddleware