import React from 'react';
import ReactDOM from 'react-dom';

export default class TestComponent extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = { value  : 0};
	}
	
	handleClick(event)
	{
		const update = () => {
			this.setState({ value : this.state.value + 1 },
				() => { this.timeout = setTimeout(update, 100) }
			
			);
		}
		
		update();
	}
	
	
	render()
	{
		return <div onClick={e => this.handleClick(e)} >
			a test, {this.props.name} : {this.state.value}
		</div>
	}
}