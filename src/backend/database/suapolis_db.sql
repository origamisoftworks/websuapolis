-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 08 Juin 2017 à 12:32
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `suapolis_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_actions`
--

CREATE TABLE `t_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_game` int(10) UNSIGNED NOT NULL,
  `id_player` int(10) UNSIGNED NOT NULL,
  `message` varchar(50) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- Erreur de lecture des données :  (#2014 - Commands out of sync; you can't run this command now)

-- --------------------------------------------------------

--
-- Structure de la table `t_game`
--

CREATE TABLE `t_game` (
  `id` int(11) UNSIGNED NOT NULL,
  `str_id` varchar(10) NOT NULL,
  `player1_id` int(11) UNSIGNED NOT NULL,
  `player2_id` int(11) UNSIGNED DEFAULT NULL,
  `finish` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Check if the game is over',
  `seed` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- Erreur de lecture des données :  (#2014 - Commands out of sync; you can't run this command now)

-- --------------------------------------------------------

--
-- Structure de la table `t_score`
--

CREATE TABLE `t_score` (
  `id` int(10) UNSIGNED NOT NULL,
  `game_id` int(11) UNSIGNED NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- Erreur de lecture des données :  (#2014 - Commands out of sync; you can't run this command now)

-- --------------------------------------------------------

--
-- Structure de la table `t_score_per_player`
--

CREATE TABLE `t_score_per_player` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_score` int(10) UNSIGNED NOT NULL,
  `id_player` int(10) UNSIGNED NOT NULL,
  `score` int(11) NOT NULL,
  `production` int(11) NOT NULL,
  `bonheur` int(11) NOT NULL,
  `argent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- Erreur de lecture des données :  (#2014 - Commands out of sync; you can't run this command now)

-- --------------------------------------------------------

--
-- Structure de la table `t_users`
--

CREATE TABLE `t_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Informations sur les divers utilisateurs';
-- Erreur de lecture des données :  (#2014 - Commands out of sync; you can't run this command now)

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_actions`
--
ALTER TABLE `t_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_game` (`id_game`),
  ADD KEY `id_player` (`id_player`);

--
-- Index pour la table `t_game`
--
ALTER TABLE `t_game`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `str_id` (`str_id`),
  ADD KEY `player1_id` (`player1_id`),
  ADD KEY `player2_id` (`player2_id`);

--
-- Index pour la table `t_score`
--
ALTER TABLE `t_score`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_id` (`game_id`);

--
-- Index pour la table `t_score_per_player`
--
ALTER TABLE `t_score_per_player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_score` (`id_score`),
  ADD KEY `id_player` (`id_player`);

--
-- Index pour la table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_actions`
--
ALTER TABLE `t_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `t_game`
--
ALTER TABLE `t_game`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_score`
--
ALTER TABLE `t_score`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `t_score_per_player`
--
ALTER TABLE `t_score_per_player`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_actions`
--
ALTER TABLE `t_actions`
  ADD CONSTRAINT `game_id.id` FOREIGN KEY (`id_game`) REFERENCES `t_game` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `player_id.id` FOREIGN KEY (`id_player`) REFERENCES `t_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `t_game`
--
ALTER TABLE `t_game`
  ADD CONSTRAINT `player1_id.id` FOREIGN KEY (`player1_id`) REFERENCES `t_users` (`id`),
  ADD CONSTRAINT `player2_id.id` FOREIGN KEY (`player2_id`) REFERENCES `t_users` (`id`);

--
-- Contraintes pour la table `t_score`
--
ALTER TABLE `t_score`
  ADD CONSTRAINT `t_score_t_game` FOREIGN KEY (`game_id`) REFERENCES `t_game` (`id`);

--
-- Contraintes pour la table `t_score_per_player`
--
ALTER TABLE `t_score_per_player`
  ADD CONSTRAINT `t_sscore` FOREIGN KEY (`id_score`) REFERENCES `t_score` (`id`),
  ADD CONSTRAINT `t_suser` FOREIGN KEY (`id_player`) REFERENCES `t_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
