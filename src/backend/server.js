// import express and modules associated
var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var minify = require('express-minify');

var async = require('async');

var app = express();

// apply modules on express.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(minify());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// import mysql
var mysql = require('mysql');

var config = require('./config');

// Database connection
var connection = mysql.createConnection({
    host: config["host"],
    user: config["user"],
    password: config["password"],
    database: config["database"],
    multipleStatements: true
	});

function Authentification(username, password, callback)
{
	query = "SELECT * FROM t_users WHERE user_name = " + connection.escape(username) + " AND password = " + connection.escape(password);
	/**
     ** Bugged part of mysql module
     ** (src: https://stackoverflow.com/questions/14087924/cannot-enqueue-handshake-after-invoking-quit)
     */
	//connection.connect();
	connection.query(query, function(err, rows, fields) {
        if (err){
        	console.log("error(l36) : Database error");
        	callback(-1);
        }

        /**
         ** Bugged part of mysql module
         ** (src: https://stackoverflow.com/questions/14087924/cannot-enqueue-handshake-after-invoking-quit)
         */
        //connection.end();

        if (rows.length())
            callback(rows[0].id);
        else
        	callback(-1);
    });
}

// Authentification post method.
app.post('/api/authentification', function (req, res) {
	var user = req.body.user;
	var password = req.body.password;
	console.log("user: " + user + " ,password: "+ password);
	Authentification(user, password, function(response) {
		obj = {};
		obj['result'] = response;
		console.log("Sending: " + obj);
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(obj));
	});
});

function GamePlayerGetter(game_id, callback)
{
    query = "SELECT player.id, player.user_name, game.finish, game.str_id FROM t_users player "
        + "JOIN t_game game ON player.id = game.player1_id OR player.id = game.player2_id "
        + "WHERE game.id = " + game_id;
    //query = "SELECT player1_id, player2_id, finish FROM t_game WHERE t_game.id = " + game_id;
    connection.query(query, function(err, rows, fields) {
        if (err){
            console.log(err.message);
            callback(-1);
        }

        if (rows.length)
            callback({
                "isfinish" : rows[0]["finish"],
                "game_id" : rows[0]["str_id"],
                "player" : {
                    "j1": {
                        "id": rows[0]["id"],
                        "pseudo": rows[0]["user_name"]
                    },
                    "j2": {
                        "id": rows[1]["id"],
                        "pseudo": rows[1]["user_name"]
                    }
                }
            });
        else
            callback(-1);
    });
}

function GameScoreGetter(game_id, j1_id, j2_id, callback)
{
    query = "SELECT score.time, spp.id_player, spp.score, spp.production, spp.bonheur, spp.argent FROM t_score score "
        + "JOIN t_score_per_player spp ON score.id = spp.id_score "
        + "WHERE score.game_id = " + game_id;
    connection.query(query, function(err, rows, fields) {
        if (err) {
            console.log(err.message);
            callback(-1);
        }

        if (rows.length) {
            var statime = [];
            var value = {};
            rows.forEach(function (elem) {
                //console.log(elem);
                if (!value[elem["time"]]) {
                    value[elem["time"]] = {
                        "date": elem["time"]
                    };
                }
                if (elem["id_player"] === j1_id && !value[elem["time"]]["j1"])
                    value[elem["time"]]["j1"] = {
                        "score" : elem["score"],
                        "money" : elem["argent"],
                        "production" : elem["production"],
                        "bonheur" : elem["bonheur"],
                        "argent" : elem["argent"]
                    };
                else if (elem["id_player"] === j2_id && !value[elem["time"]]["j2"]) {
                    value[elem["time"]]["j2"] = {
                        "score" : elem["score"],
                        "money" : elem["argent"],
                        "production" : elem["production"],
                        "bonheur" : elem["bonheur"],
                        "argent" : elem["argent"]
                    };
                }
            });
            for (key in value)
                statime.push(value[key]);
            callback(statime);
        }
        else
            callback([]);
    });
}

function GameBuildingGetter(game_id, callback){
    query = "SELECT id_player, message, date FROM t_actions WHERE id_game = " + game_id;
    connection.query(query, function(err, rows, fields) {
        if (err){
            console.log(err.message);
            callback(-1);
        }

        if (rows.length) {
            var buildings = [];
            rows.forEach(function(elem) {
                var data = elem["message"].split(",");
                if (data[0] === "new") {
                    buildings.push( {
                        "type": data[1],
                        "id": data[2],
                        "player": elem["id_player"],
                        "action": data[0],
                        "pos": {
                            "x": data[3],
                            "y": data[4],
                            "z": data[5]
                        },
                        "construction_date" : elem["date"],
                        "destruction_date" : null
                    });
                }
                else if (data[0] === "delete" && buildings[data[1]]) {
                    buildings[data[1]]["destruction_date"] = elem["date"];
                }
            });
            callback(buildings);
        }
        else
            callback([]);
    });
}

var filepath = "C:\\photon\\Photon-OnPremise-Server-SDK_v4-0-29-11263\\deploy\\Lightning" //TODO: To change when deploy

app.get('/api/game/:id', function(req, res) {
	var id = req.params.id;
	GamePlayerGetter(id, function(result){
	    if (result === -1) {
            res.status(500).send('GPG: Invalid player');
        }
        else {
            var global = result;
            GameScoreGetter(id, global["player"]["j1"]["id"], global["player"]["j2"]["id"], function (result_score) {
                if (result_score === -1) {
                    res.status(500).send('GSG: Problem with the score');
                }
                else {
                    global["statime"] = result_score;

                    GameBuildingGetter(id, function (result_building) {
                        if (result_building === -1)
                            res.status(500).send('GBG: Invalid query on building');
                        global["building"] = result_building;
                        //console.log(global);

                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify(global));
                    })
                }
            })
        }
    })
});



app.get('/api/game/image/:id', function(req, res) {
	var id = req.params.id;
	res.setHeader('Content-Type', 'image/jpg');
	res.sendFile(filepath + "\\" + id + '.jpg')
});

function FinishedGamePerPlayer(id_player, callback) {
    query = "SELECT game.id AS game_id, player.user_name, player.id AS player_id, spp.score, score.time, spp.production, spp.bonheur, spp.argent FROM t_game game "
        + "JOIN t_users player ON game.player1_id = player.id OR game.player2_id = player.id "
        + "JOIN t_score score ON game.id = score.game_id "
        + "JOIN t_score_per_player spp ON score.id = spp.id_score AND player.id = spp.id_player "
        + "WHERE player.id =" + id_player + " "
        + "ORDER BY score.time DESC";
    connection.query(query, function(err, rows, fields) {
        if (err) {
            console.log(err.message);
            callback(-1);
        }

        if (rows.length)
        {
            let value = {"player" : {}, "game" : []};
            let tmp = {};
            let score = 0;
            let score_num = 0;
            let production = 0;
            let production_num = 0;
            let bonheur = 0;
            let bonheur_num = 0;
            let argent = 0;
            let argent_num = 0;
            rows.forEach(function(elem) {
                if (!value["player"]["id"])
                {
                    value["player"]["id"] = elem["player_id"];
                    value["player"]["pseudo"] = elem["user_name"];
                    value["player"]["best_score"] = 0;
                }
                if (!tmp[elem["game_id"]])
                {
                    tmp[elem["game_id"]] = {
                        "id_game" : elem["game_id"],
                        "start_time" : elem["time"],
                        "current_score" : elem["score"]
                    };
                    console.log(elem["score"] + ">?" + value["player"]["best_score"] );
                    if (elem["score"] > value["player"]["best_score"]) {
                        value["player"]["best_score"] = elem["score"];
                    }
                }
                score += elem["score"];
                score_num += 1;
                production += elem["production"];
                production_num += 1;
                bonheur += elem["bonheur"];
                bonheur_num += 1;
                argent += elem["argent"];
                argent_num += 1;
            });
            value["player"]["average"] = {
                "score" : (score/score_num).toFixed(2),
                "production" : (production/production_num).toFixed(2),
                "bonheur" : (bonheur/bonheur_num).toFixed(2),
                "argent" : (argent/argent_num).toFixed(2)
            };
            for (key in tmp)
                value["game"].push(tmp[key]);
            callback(value);
        }
        else
            callback(-1);
    });
}

app.get('/api/player/:id', function (req, res) {
    let id = req.params.id;
    FinishedGamePerPlayer(id, function (result) {
        if (result === -1) {
            res.status(500).send('FGP: Problem with query on player');
        }
        else {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(result));
        }
    });
});

function NonFinishedGameGetter(callback) {
    query = "SELECT game.id AS game_id, player.user_name, player.id AS player_id, spp.score, score.time FROM t_game game "
        + "JOIN t_users player ON game.player1_id = player.id OR game.player2_id = player.id "
        + "JOIN t_score score ON game.id = score.game_id "
        + "JOIN t_score_per_player spp ON score.id = spp.id_score AND player.id = spp.id_player "
        + "WHERE game.finish = 0 "
        + "ORDER BY score.time DESC";
    connection.query(query, function(err, rows, fields) {
        if (err) {
            console.log(err.message);
            callback(-1);
        }

        if (rows.length)
        {
            let value = {};
            rows.forEach(function(elem){
                if(!value[elem["game_id"]])
                {
                    value[elem["game_id"]] = {
                        "id_game" : elem["game_id"],
                        "start_time" : elem["time"]
                    };
                }
                else if (value[elem["game_id"]]["start_time"] > elem["time"])
                {
                    value[elem["game_id"]]["start_time"] = elem["time"];
                }
                if(!value[elem["game_id"]]["player1"])
                {
                    value[elem["game_id"]]["player1"] = {
                        "pseudo" : elem["user_name"],
                        "id" : elem["player_id"],
                        "current_score" : elem["score"]
                    };
                }
                else if (value[elem["game_id"]]["player1"]
                         && !value[elem["game_id"]]["player2"]
                         && elem["player_id"] !== value[elem["game_id"]]["player1"]["id"])
                {
                    value[elem["game_id"]]["player2"] = {
                        "pseudo" : elem["user_name"],
                        "id" : elem["player_id"],
                        "current_score" : elem["score"]
                    };
                }

            });
            let data = { "games" : []};
            for (key in value)
                data["games"].push(value[key]);
            data["game_number"] = data["games"].length;
            callback(data);
        }
        else
            callback(-1);
    });
}

app.get('/api/game', function(req, res) {
    NonFinishedGameGetter(function (result) {
        if (result === -1) {
            res.status(500).send('NFGG: Problem with query on player');
        }
        else {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(result));
        }
    });
});

function LeaderBoardGetter(callback)
{
    query = "SELECT c.date as time, t_score_per_player.score as score, c.id_player as id_player, t_users.user_name as user_name"
    + " FROM t_score_per_player"
    + " LEFT JOIN t_score ON t_score_per_player.id_score = t_score.id"
    + " LEFT JOIN t_users ON t_users.id = t_score_per_player.id_player"
    + " INNER JOIN ("
    + " SELECT MAX(b.date) as date, b.id_player as id_player"
    + " FROM t_users"
    + " INNER JOIN ("
    + " SELECT `t_score_per_player`.`score` as score, `t_score_per_player`.`id_player` as id_player, a.max_time as date, a.game as game, `t_score_per_player`.`id` as id_score"
    + " FROM `t_score_per_player`"
    + " INNER JOIN ("
    + " SELECT MAX(`t_score`.`time`) as max_time, `t_score`.`game_id` as game"
    + " FROM `t_score`"
    + " GROUP BY `t_score`.`game_id`"
    + " )a"
    + " LEFT JOIN `t_score` ON `t_score_per_player`.`id_score` = `t_score`.`id`"
    + " WHERE `t_score`.`time` = a.max_time"
    + " )b"
    + " WHERE t_users.id = b.id_player"
    + " GROUP BY b.id_player"
    + " )c"
    + " WHERE t_score.time = c.date AND t_score_per_player.id_player = c.id_player"
	+ " ORDER BY score DESC";
    connection.query(query, function(err, rows, fields) {
        if (err) {
            console.log(err.message);
            callback(-1);
        }

        if (rows.length)
        {
            let data = {};
            let placement = 0;
            rows.forEach(function(elem) {
                if (!data[elem["user_name"]])
                {
                    data[elem["user_name"]] = {
                        "place" : placement,
                        "id" : elem["id_player"],
                        "name" : elem["user_name"],
                        "score" : elem["score"],
                        "date" : elem["time"]
                    };
                    ++placement;
                }
                else if (data[elem["user_name"]] && data[elem["user_name"]]["date"] < elem["time"]){
                    //console.log("date problem");
                }
            });
            var player = { "players" : []};
            for (key in data)
                player["players"].push(data[key]);
            callback(player);
        }
        else
            callback(-1);
    });
}

app.get('/api/leaderboard', function (req, res){
    LeaderBoardGetter(function (result) {
        if (result === -1) {
            res.status(500).send('LBG: Problem with query in leaderboard');
        }
        else {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(result));
        }
    })
});



app.listen(4242);